# django-component changelog

## 0.1.7

* Support deep arg tags
* Support media and arg tags registration within cache
* Django 3.1 support

## 0.1.6

* Support media registration through extended template

## 0.1.5

* Multiple use of the same arg tag is passed as a list

## 0.1.4

* slots is removed in favor of arg
* Remove use_components tag, and support media tags after components
* All components are both registered both as self-closed and as not sefl-closed
* Render components in the order they are encountered, instead of reverse order

## 0.1.2

* Support nested components
