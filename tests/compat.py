from django import VERSION as DJ_VERSION

type_js = "" if DJ_VERSION >= (3, 1) else 'type="text/javascript"'
