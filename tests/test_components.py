from django.core.cache import cache
from django.template import Context, Template, loader
from django.test import SimpleTestCase

from .compat import type_js
from .django_settings import *


class TemplateTagTest(SimpleTestCase):
    def setUp(self) -> None:
        cache.clear()

    def _test_template(self, template_str, expected_html):
        template = Template(template_str)
        rendered = template.render(Context())
        self.assertHTMLEqual(rendered, expected_html)

    def test_children(self):
        self._test_template(
            """
                {% load test_components %}
                {% Children %}
                    <p>Content</p>
                {% /Children %}
            """,
            """
                <div>
                    <p>Content</p>
                </div>
            """,
        )

    def test_context(self):
        self._test_template(
            "{% load test_components %}{% Context/ number=5 %}", "<p>5 * 5 = 25</p>",
        )

    def test_context_scope(self):
        self._test_template(
            "{% load test_components %}{% ContextScope value=5 %}{% ContextScope/ value=1 %}{% /ContextScope %}",
            "1 5",
        )

    def test_media(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Media/ %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <script {} src="media.js"></script>
            """.format(type_js),
        )

    def test_multiple_media(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Media/ %}
                {% MediaExternal/ %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <link href="https://example.com/media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <div></div>
            <script {} src="media.js"></script>
            <script {} src="https://example.com/media.js"></script>
            """.format(type_js, type_js),
        )

    def test_media_after(self):
        self._test_template(
            """
                {% load test_components %}
                {% Media/ %}
                {% components_css %}
                {% components_js %}
            """,
            """
            <div></div>
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <script {} src="media.js"></script>
            """.format(type_js),
        )

    def test_prop(self):
        self._test_template(
            "{% load test_components %}{% Prop/ prop='test' %}", "<p>test</p>",
        )

    def test_arg_tag(self):
        self._test_template(
            """
                {% load test_components %}
                {% Arg %}
                    {% arg title %}
                        <span>Arg</span> title
                    {% endarg %}
                {% /Arg %}
            """,
            """
                <div>
                    <h1><span>Arg</span> title</h1>
                </div>
            """,
        )

    def test_arg_tag_list(self):
        self._test_template(
            """
                {% load test_components %}
                {% ArgList %}
                    {% arg elements %}
                        <span>Test</span>
                    {% endarg %}
                    {% arg elements %}
                        <span>Test</span>
                    {% endarg %}
                {% /ArgList %}
            """,
            """
                <div>
                    <span>Test</span>
                    <span>Test</span>
                </div>
            """,
        )

    def test_arg_tag_list_forloop(self):
        self._test_template(
            """
                {% load test_components %}
                {% ArgList %}
                    {% for i in '123'|make_list %}
                        {% arg elements %}
                            <span>{{ forloop.counter }}</span>
                        {% endarg %}                        
                    {% endfor %}
                {% /ArgList %}
            """,
            """
                <div>
                    <span>1</span>
                    <span>2</span>
                    <span>3</span>
                </div>
            """,
        )

    def test_arg_tag_cache(self):
        template_str = """
                {% load test_components %}
                {% Arg %}
                    {% cache 1 arg %}
                        {% arg title %}
                            <span>Arg</span> title
                        {% endarg %}
                    {% endcache %}
                {% /Arg %}
            """
        expected_html = """
            <div>
                <h1><span>Arg</span> title</h1>
            </div>
            """
        template = Template(template_str)
        rendered = template.render(Context())
        self.assertHTMLEqual(rendered, expected_html)
        rendered = template.render(Context())
        self.assertHTMLEqual(rendered, expected_html)

    def test_nested(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Nested/ %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <script {} src="media.js"></script>
            """.format(type_js),
        )

    def test_extend_medias(self):
        self._test_template(
            loader.render_to_string("extend.html"),
            """
        <link href="media.css" type="text/css" media="all" rel="stylesheet">
        <div></div>
        <script {} src="media.js"></script>
        """.format(type_js),
        )

    def test_media_cache(self):
        template_str = """
                {% load test_components %}
                {% components_css %}
                {% cache 1 media %}
                    {% Media/ %}
                {% endcache %}
                {% components_js %}
            """
        expected_html = """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <script {} src="media.js"></script>
            """.format(type_js)
        self._test_template(template_str, expected_html)
        self._test_template(template_str, expected_html)

    def test_media_cache_advanced(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Media/ %}
                {% cache 1 media_advanced %}
                    {% MediaExternal/ %}
                {% endcache %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <link href="https://example.com/media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <div></div>
            <script {} src="media.js"></script>
            <script {} src="https://example.com/media.js"></script>
        """.format(type_js, type_js),
        )
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% cache 1 media_advanced %}
                    {% MediaExternal/ %}
                {% endcache %}
                {% components_js %}
            """,
            """
            <link href="https://example.com/media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <script {} src="https://example.com/media.js"></script>
        """.format(type_js),
        )
